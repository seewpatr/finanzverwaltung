jQuery = $ = require('jquery');
require('jquery-ui-dist/jquery-ui.js');
require('knex');

// document.write('<script src="./scripts/vendor/jquery-ui.js"></script>');
document.write('<link rel="stylesheet" href="./styles/jquery-ui.css"></link>');
const { ipcRenderer } = require('electron');
// TODO Alle exportierten Module in ein Script führen
const navigation = require('./scripts/sub/nav.js').navigation;
const login = require('./scripts/sub/login.js').login;
const finanzen = require('./scripts/sub/finanzen.js').finanzen;
const profile = require('./scripts/sub/profil.js').profil;
const configuration = require('./scripts/sub/konfiguration.js').configuration;

// TODO Server - Infos global etablieren
const { nodeServer } = '84.16.242.168:8287';

jQuery(document).ready(function () {
    // ipc.send('windowLoaded');
    // ipc.on('resultSent', function(event, result) {
    //     console.log(result);
    // });

    let current = $('meta[current]').attr('current') * 1;
    
    navigation.createMainNavigation(current);    

    switch (current) { 
        case -1:
            login.createLoginPanel();
            login.emptyStatusMessages();
            break;
        case 0:
                navigation.createTabNavigation();
                profile.createProfilContent();               
            break;
        case 1:
            navigation.createTabNavigation();
            configuration.createConfigurationMainContent();
            break;
        case 2:
             navigation.createTabNavigation();
             finanzen.createFinGeneralContent();                                       
             finanzen.createFinDetailsContent();   
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;

        default:
            break;
    }

});

