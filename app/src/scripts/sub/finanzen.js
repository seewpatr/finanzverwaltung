
exports.finanzen = {

    /**
     * Erzeugt generisch den Inhalt für den Reiter Finanzen - Übersicht 
     */
    createFinGeneralContent() {
        let table = $(`<table>`)
            .attr({
                id: `uebersichtTable`
            })
            .append($(`<thead>`)
                .append($(`<tr>`)
                    .append(
                        $(`<th>`)
                            .addClass(`alignLeft`)
                            .html(`Ziel`),
                        $(`<th>`)
                            .addClass(`alignLeft`)
                            .html(`Konto`),
                        $(`<th>`)
                            .addClass(`alignRight`)
                            .html(`Einnahmen`),
                        $(`<th>`)
                            .addClass(`alignRight`)
                            .html(`Ausgaben`),
                        $(`<th>`)
                            .addClass(`alignRight`)
                            .html(`Gesamt`)
                    )
                )
            ).appendTo(`#finGeneral`);

        // TODO Empfehlung ist später durch einfaches Chart auszutauschen
        let empfehlung = $(`<div>`).appendTo(`#finGeneral`);

        empfehlung
            .attr({
                id: `empfehlung`
            });

        let tbody = $(`<tbody>`).appendTo(table);

        console.log()
        // TODO Hier wird später dynamisch befüllt
        for (let index = 0; index < 3; index++) {
            $(`<tr>`)
                .append($(`<td>`)
                    .html(`TestZiel`)
                )
                .append($(`<td>`)
                    .html(`TestKonto`)
                )
                .append($(`<td>`)
                    .html(`5,00`)
                    .addClass(`alignRight`)
                )
                .append($(`<td>`)
                    .html(`4,00`)
                    .addClass(`alignRight`)
                )
                .append($(`<td>`)
                    .html(`1,00`)
                    .addClass(`alignRight`)
                )
                .appendTo(tbody);
        }
        // TODO hier wird später dynamisch berechnet
        $(`<tfoot>`)
            .append($(`<tr>`)
                .append($(`<td>`)
                    .html(`Gesamtsaldo`)
                    .attr({
                        colspan: 2
                    })
                )
                .append($(`<td>`)
                    .addClass(`plus`)
                    .html(`15,00`)
                    .addClass(`alignRight`)
                )
                .append($(`<td>`)
                    .addClass(`minus`)
                    .html(`12,00`)
                    .addClass(`alignRight`)
                )
                .append($(`<td>`)
                    .html(`3,00`)
                    .addClass(`alignRight`)
                )
            ).appendTo(table);
    },
    /**
     * Ruft Methoden auf, die zusammen den gesamten Inhalt für den Reiter Finanzen - Details erzeugen
     */
    createFinDetailsContent() {
        this.createFinDetailsOverview();
        this.createFinDetailsSorting();
        this.createFinDetailsPagination();
        this.createFinDetailsTable();
        this.toggleSorting();
    },
    /**
     * Erzeugt generisch die Tabelle für den Reiter Finanzen - Details
     */
    createFinDetailsTable() {

        let table = $(`<table>`)
            .attr({
                id: `detailsTable`
            })
            .append($(`<thead>`)
                .append($(`<tr>`)
                    .append(
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`#`),
                        $(`<th>`)
                            .addClass(`alignCenter sort`)
                            .html(`Typ`),
                        $(`<th>`)
                            .addClass(`alignCenter sort`)
                            .html(`Kategorie`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`Betrag`),
                        $(`<th>`)
                            .addClass(`alignCenter sort`)
                            .html(`Ziel`),
                        $(`<th>`)
                            .addClass(`alignCenter sort`)
                            .html(`Intervall`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`Datum`),
                        $(`<th>`)
                            .addClass(`alignLeft`)
                            .html(`Kommentar`),
                        $(`<th>`)
                            .addClass(`alignCenter`)
                            .html(`Ändern`),
                        $(`<th>`)
                            .addClass(`alignCenter`)
                            .html(`Löschen`)
                    )
                )
            ).appendTo(`#finDetails`);

        let tbody = $(`<tbody>`).appendTo(table)

        // TODO Hier wird später dynamisch befüllt
        for (let index = 1; index <= 10; index++) {
            $(`<tr>`)
                .append(
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`${index}`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`Sparkonto`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`Haushalt`),
                    $(`<td>`)
                        .addClass(`alignRight`)
                        .html(`23,50`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`Easybank`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`Monatlich`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`17.05.2019`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`Mir fällt nichts ein`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .append(
                            $(`<button>`)
                                .addClass('iconBtn editDetailEntryBtn')
                                .attr(`data-type`, `add-${index}`)
                                .on('click', this.editDetailEntry)
                        ),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .append(
                            $(`<button>`)
                                .addClass('iconBtn deleteDetailEntryBtn')
                                .attr(`data-type`, `delete-${index}`)
                                .on('click', this.deleteDetailEntry)
                        )
                )

                .appendTo(tbody);
        }
        // TODO Hier wird später dynamisch befüllt
        let newEntry = this.createFinDetailsNewEntryInput();
        newEntry.appendTo(tbody);
    },
    /**
     * Erzeugt generisch die kleine Übersicht für den Reiter Finanzen - Details
     */
    createFinDetailsOverview() {
        let sum = $(`<table>`).appendTo(`#finDetails`);

        sum
            .attr({
                id: 'detailSum'
            })
            .append(
                $(`<thead>`)
                    .append(
                        $(`<tr>`)
                            .append(
                                $(`<th>`)
                                    .addClass(`alignLeft`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(' '),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`Einnahmen`),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`Ausgaben`),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`Gesamt`)
                            )
                    )
            );
        // TODO Hier wird später dynamisch befüllt
        sum
            .append(
                $(`<thead>`)
                    .append(
                        $(`<tr>`)
                            .append(
                                $(`<th>`)
                                    .addClass(`alignLeft`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html('Gesamtkapital'),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`1000,00`),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`500,00`),
                                $(`<th>`)
                                    .addClass(`alignRight`)
                                    .attr({
                                        colspan: 2
                                    })
                                    .html(`500,00`)
                            )
                    )
            );
    },
    /**
     * Erzeugt generisch die Sortierung für den Reiter Finanzen - Details
     */
    createFinDetailsSorting() {
        let sorting = $(`<div>`).appendTo(`#finDetails`);

        // TODO Hier wird später dynamisch befüllt
        sorting
            .attr({
                id: `detailSorting`
            })
            .append(
                $('<form>')
                    .append(
                        $('<h1>')
                            .html('Suchen & Filtern')
                            .addClass('alignCenter'),
                        $('<hr>'),
                        $('<select>')
                            .attr(`data-type`, `select-type`)
                            .append(
                                $(`<option>`)
                                    .attr({
                                        value: `type`,
                                        selected: `selected`,
                                        disabled: `disabled`
                                    })
                                    .html(`Typ`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Einnahmen`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Ausgaben`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Umbuchung`)
                            ),
                        $(`<select>`)
                            .attr(`data-type`, `select-cat`)
                            .append(
                                $(`<option>`)
                                    .attr({
                                        value: `type`,
                                        selected: `selected`,
                                        disabled: `disabled`
                                    })
                                    .html(`Kategorie`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 1`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 2`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 3`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 4`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 5`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 6`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 7`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 8`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 9`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 10`)
                            ),
                        $('<input>')
                            .attr({
                                type: 'number',
                                placeholder: 'Betrag von',
                                'data-type': 'select-amountMin'
                            }),
                        $('<input>')
                            .attr({
                                type: 'number',
                                placeholder: 'Betrag bis',
                                'data-type': 'select-amountMax'
                            }),
                        $(`<select>`)
                            .attr(`data-type`, `select-aim`)
                            .append(
                                $(`<option>`)
                                    .attr({
                                        value: `type`,
                                        selected: `selected`,
                                        disabled: `disabled`
                                    })
                                    .html(`Ziel`),
                                $(`<option>`)
                                    .attr({
                                        value: `konto-1`
                                    })
                                    .html(`Konto 1`),
                                $(`<option>`)
                                    .attr({
                                        value: `konto-2`
                                    })
                                    .html(`Konto 2`),
                                $(`<option>`)
                                    .attr({
                                        value: `konto-3`
                                    })
                                    .html(`Konto 3`)
                            ),
                        $('<input>')
                            .attr({
                                type: 'date',
                                //TODO actual Date einfügen
                                value: new Date(),
                                'date-type': 'select-dateMin'
                            }).datepicker(),
                        $('<input>')
                            .attr({
                                type: 'date',
                                //TODO actual Date einfügen
                                value: new Date(),
                                'date-type': 'select-dateMax'
                            }).datepicker(),
                        $('<input>')
                            .attr({
                                type: 'text',
                                placeholder: 'Stichwortsuche',
                                'date-type': 'select-custom'
                            }),
                        $('<button>')
                            .html('Zurücksetzen')
                            .attr({
                                type: 'reset'
                            })
                            .on('click', function (e) {
                                e.preventDefault()
                            })
                            .button(),
                        $('<button>')
                            .html('Suchen')
                            .attr({
                                type: 'submit'
                            })
                            .on('click', function (e) {
                                e.preventDefault();
                            })
                            .button()
                    )
            );
    },
    /**
     * Erzeugt generisch die Paginierung für den Reiter Finanzen - Details
     */
    createFinDetailsPagination() {
        let pagination = $(`<div>`).appendTo(`#finDetails`);

        pagination
            .attr({
                id: `detailPagination`
            })
            .append(
                $('<select>')
                    .attr(`data-type`, `pag-count`)
                    .append(
                        $(`<option>`)
                            .attr({
                                value: `10`,
                                selected: `selected`
                            })
                            .html(`10 Einträge`),
                        $(`<option>`)
                            .attr({
                                value: `20`
                            })
                            .html(`20 Einträge`),
                        $(`<option>`)
                            .attr({
                                value: `50`
                            })
                            .html(`50 Einträge`),
                        $(`<option>`)
                            .attr({
                                value: `all`
                            })
                            .html(`Alle Einträge`)
                    )
            )
            // TODO Datatables für Sortierung und Paginierung
            .append(
                $('<span>')
                    .html('PAGINIERUNG FOLGT NOCH')
            );
    },
    /**
     * Erzeugt eine Zeile zum Einfügen eines neuen Eintrags für die Detail-Tabelle im Reiter Finanzen - Details
     */
    createFinDetailsNewEntryInput() {
        return $(`<tr>`)
            .append(
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .html(`#`),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append($(`<select>`)
                        .attr(`data-type`, `select-type`)
                        .append(
                            $(`<option>`)
                                .attr({
                                    value: `type`,
                                    selected: `selected`,
                                    disabled: `disabled`
                                })
                                .html(`Typ`),
                            $(`<option>`)
                                .attr({
                                    value: `type`
                                })
                                .html(`Einnahmen`),
                            $(`<option>`)
                                .attr({
                                    value: `type`
                                })
                                .html(`Ausgaben`),
                            $(`<option>`)
                                .attr({
                                    value: `type`
                                })
                                .html(`Umbuchung`)
                        )
                    ),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append(
                        $(`<select>`)
                            .attr(`data-type`, `add-cat`)
                            .append(
                                $(`<option>`)
                                    .attr({
                                        value: `type`,
                                        selected: `selected`,
                                        disabled: `disabled`
                                    })
                                    .html(`Kategorie`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 1`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 2`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 3`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 4`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 5`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 6`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 7`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 8`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 9`),
                                $(`<option>`)
                                    .attr({
                                        value: `type`
                                    })
                                    .html(`Kat 10`)
                            )
                    ),
                $(`<td>`)
                    .addClass(`alignRight`)
                    .append(
                        $('<input>')
                            .attr({
                                type: 'number',
                                placeholder: 'Betrag',
                                'data-type': 'select-amount'
                            })
                    ),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append($(`<select>`)
                        .attr(`data-type`, `select-aim`)
                        .append(
                            $(`<option>`)
                                .attr({
                                    value: `type`,
                                    selected: `selected`,
                                    disabled: `disabled`
                                })
                                .html(`Ziel`),
                            $(`<option>`)
                                .attr({
                                    value: `konto-1`
                                })
                                .html(`Konto 1`),
                            $(`<option>`)
                                .attr({
                                    value: `konto-2`
                                })
                                .html(`Konto 2`),
                            $(`<option>`)
                                .attr({
                                    value: `konto-3`
                                })
                                .html(`Konto 3`)
                        )
                    ),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append($(`<select>`)
                        .attr(`data-type`, `add-type`)
                        .append(
                            $(`<option>`)
                                .attr({
                                    value: `type`,
                                    selected: `selected`,
                                    disabled: `disabled`
                                })
                                .html(`Intervall`),
                            $(`<option>`)
                                .attr({
                                    value: `intervall-once`
                                })
                                .html(`Einmalig`),
                            $(`<option>`)
                                .attr({
                                    value: `intervall-day`
                                })
                                .html(`Täglich`),
                            $(`<option>`)
                                .attr({
                                    value: `intervall-month`
                                })
                                .html(`Monatlich`),
                            $(`<option>`)
                                .attr({
                                    value: `intervall-year`
                                })
                                .html(`Jährlich`)
                        )
                    ),
                $(`<td>`)
                    .addClass(`alignLeft`)
                    .append(
                        $('<input>')
                            .attr({
                                type: 'date',
                                //TODO actual Date einfügen
                                value: new Date(),
                                'date-type': 'select-date'
                            }).datepicker()
                    ),
                $(`<td>`)
                    .addClass('alignLeft')
                    .append(
                        $('<input>')
                            .attr({                                
                                placeholder: `Kein Kommentar`
                            })
                    ),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append(
                        $(`<button>`)
                            .attr(`data-type`, `addComm`)
                    ),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append(
                        $(`<button>`)
                            .addClass('iconBtn addDetailEntryBtn')
                            .attr(`data-type`, `newRecord`)
                    )
            )
    },
    /**
     * Switcht zwischen aufsteigender und absteigender Reihenfolge
     */
    toggleSorting() {
        // TODO wird später noch auf echte Sortierung umgebaut
        $('th.sort').on('click', function () {
            $(this).toggleClass('sortDesc');
        });
    },

    /**
     * Erzeugt einen Dialog zum Editieren eines Eintrags der Details-Tabelle im Reiter Finanzen - Details
     */
    editDetailEntry() {
        // TODO Verbesserungspotenzial
        let entry = $(this).parent().parent().parent();

        let editEntry = $('<div>')
            .attr({
                id: 'editEntry'
            })
            .append(
                $('<table>')
                    .append(
                        $('<thead>')
                            .append($(`<tr>`)
                                .append(
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`#`),
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`Typ`),
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`Kategorie`),
                                    $(`<th>`)
                                        .addClass(`alignRight`)
                                        .html(`Betrag`),
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`Ziel`),
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`Intervall`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`Datum`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`Kommentar`),
                                )
                            ),
                        $('<tbody>')
                            .append(
                                $(`<tr>`)
                                    .append(
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .html(`${entry.children().eq(0).html()}`),
                                        // TODO später richtige Auswahl anzeigen
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .append($(`<select>`)
                                                .attr(`data-type`, `select-type`)
                                                .append(
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`,
                                                            selected: `selected`,
                                                            disabled: `disabled`
                                                        })
                                                        .html(`Typ`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`
                                                        })
                                                        .html(`Einnahmen`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`
                                                        })
                                                        .html(`Ausgaben`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`
                                                        })
                                                        .html(`Umbuchung`)
                                                )
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .append(
                                                $(`<select>`)
                                                    .attr(`data-type`, `add-cat`)
                                                    .append(
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`,
                                                                selected: `selected`,
                                                                disabled: `disabled`
                                                            })
                                                            .html(`Kategorie`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 1`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 2`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 3`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 4`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 5`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 6`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 7`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 8`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 9`),
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`
                                                            })
                                                            .html(`Kat 10`)
                                                    )
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignRight`)
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'number',
                                                        'data-type': 'select-amount'
                                                    })
                                                    // TODO Betrag einfügen
                                                    .val(`${entry.children().eq(3).html()}`)
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .append($(`<select>`)
                                                .attr(`data-type`, `select-aim`)
                                                .append(
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`,
                                                            selected: `selected`,
                                                            disabled: `disabled`
                                                        })
                                                        .html(`Ziel`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `konto-1`
                                                        })
                                                        .html(`Konto 1`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `konto-2`
                                                        })
                                                        .html(`Konto 2`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `konto-3`
                                                        })
                                                        .html(`Konto 3`)
                                                )
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .append($(`<select>`)
                                                .attr(`data-type`, `add-type`)
                                                .append(
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `type`,
                                                            selected: `selected`,
                                                            disabled: `disabled`
                                                        })
                                                        .html(`Intervall`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `intervall-once`
                                                        })
                                                        .html(`Einmalig`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `intervall-day`
                                                        })
                                                        .html(`Täglich`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `intervall-month`
                                                        })
                                                        .html(`Monatlich`),
                                                    $(`<option>`)
                                                        .attr({
                                                            value: `intervall-year`
                                                        })
                                                        .html(`Jährlich`)
                                                )
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignLeft`)
                                            .append(
                                                $('<input>')
                                                    // TODO Datum einfügen
                                                    .val(`${entry.children().eq(6).html()}`)
                                                    .attr({
                                                        type: 'date',
                                                        'date-type': 'select-date'
                                                    }).datepicker()
                                            ),
                                        $(`<td>`)
                                            .addClass('alignLeft')
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'textarea'
                                                    })
                                                    .val(`${entry.children().eq(7).html()}`)
                                            )
                                    )
                            )
                    )
            );
        editEntry.appendTo('#finDetails');

        editEntry
            .dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 'auto',
                modal: true,
                title: 'Eintrag bearbeiten',
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                    },
                    'Speichern': function () {
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            });
    }
    //,

    // deleteDetailEntry() {
    //     let entry = $(this).parent().parent().parent().clone().children().splice(9,1);
    //     console.log(entry);



    //     let deleteEntry = $('<div>')
    //         .attr({
    //             id: 'deleteEntry'
    //         })
    //         .append(
    //             $('<table>')
    //                 .attr({
    //                     id: 'deleteEntryTable'
    //                 })
    //                 .append(
    //                     $('<tbody>')
    //                         .append(entry)
    //                 )
    //         );

    //     deleteEntry.appendTo('#finDetails');


    //     deleteEntry
    //         .dialog({
    //             autoOpen: true,
    //             resizable: false,
    //             height: "auto",
    //             width: 'auto',
    //             modal: true,
    //             title: 'Eintrag löschen',
    //             buttons: {
    //                 'Abbrechen': function () {
    //                     $(this).dialog("close");
    //                 },
    //                 'Endgültig Löschen': function () {
    //                     $(this).dialog("close");
    //                 }
    //             },
    //             show: {
    //                 effect: "fade",
    //                 duration: 500
    //             },
    //             hide: {
    //                 effect: "fade",
    //                 duration: 500
    //             }
    //         });





    // }

}
