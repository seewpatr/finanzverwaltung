exports.configuration = {
    /**
  * Erzeugt generisch den Inhalt für den Reiter Konfiguration - Allgemein 
  */
    createConfigurationMainContent() {
        // TODO Hier wird später dynamisch befüllt und erzeugt auf Basis der Empfehlung
        let recommendation = $('<div>')
            .attr({
                id: 'recommendation'
            })
            .append(
                $('<h2>')
                    .html('Empfehlung'),
                $('<div>'),
                $('<button>')
                    .html('Anpassen')
                    .on('click', function (e) {
                        e.preventDefault();
                        $('#editRecomm').dialog('open');

                    })
                    .button()
            );

        let editRecommendation = $('<div>')
            .attr({
                id: 'editRecomm'
            })
            // TODO Hier wird später dynamisch befüllt
            .append(
                $('<form>')
                    .append(
                        $('<label>')
                            .attr({
                                id: 'recommErrorMessage'
                            }),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configRecomm1`
                                })
                                .html(`Sparen`),
                            $('<input>')
                                .attr({
                                    type: 'number',
                                    id: 'configRecomm1',
                                    value: 30
                                })
                                .addClass('recommendation')
                                .on('keyup', this.calcRecommSum)
                        ),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configRecomm2`
                                })
                                .html(`Freizeit`),
                            $('<input>')
                                .attr({
                                    type: 'number',
                                    id: 'configRecomm2',
                                    value: 50
                                })
                                .addClass('recommendation')
                                .on('keyup', this.calcRecommSum)
                        ),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configRecomm3`
                                })
                                .html(`Alltag`),
                            $('<input>')
                                .attr({
                                    type: 'number',
                                    id: 'configRecomm3',
                                    value: 20
                                })
                                .addClass('recommendation')
                                .on('keyup', this.calcRecommSum)
                        ),
                        $('<hr>'),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configRecommSum`
                                })
                                .html(`Gesamt`),
                            $('<input>')
                                .attr({
                                    type: 'text',
                                    disabled: 'disabled',
                                    id: 'configRecommSum'
                                }))
                    ),
                $('<div>')
            ).ready(this.calcRecommSum);

        // Hier wird das Modal für die Empfehlung erzeugt
        editRecommendation
            .dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 1000,
                modal: true,
                title: 'Empfehlung bearbeiten',
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                    },
                    'Speichern': function () {
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            });

        // TODO Hier wird später dynamisch befüllt und aus der DB geholt
        let form =
            $('<div>').append(
                $('<form>')
                    .append(
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configVersionId`
                                })
                                .html(`Version`),
                            $('<input>')
                                .attr({
                                    type: 'text',
                                    disabled: 'disabled',
                                    value: 'TestId1'
                                })),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configLanguageEdit`
                                })
                                .html(`Systemsprache`),
                            $(`<select>`)
                                .attr(`data-type`, `select-lang`)
                                .append(
                                    $(`<option>`)
                                        .attr({
                                            value: `de`,
                                            selected: `selected`,
                                            disabled: `disabled`
                                        })
                                        .html(`Deutsch`),
                                    $(`<option>`)
                                        .attr({
                                            value: `en`
                                        })
                                        .html(`English`),
                                )
                        ),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configCurrency`
                                })
                                .html(`Währung`),
                            $(`<select>`)
                                .attr(`data-type`, `select-curr`)
                                .append(
                                    $(`<option>`)
                                        .attr({
                                            value: `eur`,
                                            selected: `selected`,
                                            disabled: `disabled`
                                        })
                                        .html(`Euro`),
                                    $(`<option>`)
                                        .attr({
                                            value: `usd`
                                        })
                                        .html(`Dollar`),
                                    $(`<option>`)
                                        .attr({
                                            value: `gbp`
                                        })
                                        .html(`Pfund`),
                                    $(`<option>`)
                                        .attr({
                                            value: `chf`
                                        })
                                        .html(`Schweizer Franken`)
                                )
                        ),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configLogout`
                                })
                                .html(`Automatisch abmelden `),
                            $(`<select>`)
                                .attr(`data-type`, `select-logout`)
                                .append(
                                    $(`<option>`)
                                        .attr({
                                            value: `10`,
                                            selected: `selected`,
                                            disabled: `disabled`
                                        })
                                        .html(`nach 10 Minuten`),
                                    $(`<option>`)
                                        .attr({
                                            value: `30`
                                        })
                                        .html(`nach 30 Minuten`),
                                    $(`<option>`)
                                        .attr({
                                            value: `60`
                                        })
                                        .html(`nach 60 Minuten`),
                                    $(`<option>`)
                                        .attr({
                                            value: `-1`
                                        })
                                        .html(`Niemals`)
                                )
                        ),
                        $('<div>').append(
                            $('<label>')
                                .attr({
                                    for: `configTheme`
                                })
                                .html(`Theme`),
                            $(`<select>`)
                                .attr(`data-type`, `select-theme`)
                                .append(
                                    $(`<option>`)
                                        .attr({
                                            value: `light`,
                                            selected: `selected`,
                                            disabled: `disabled`
                                        })
                                        .html(`Light`),
                                    $(`<option>`)
                                        .attr({
                                            value: `dark`
                                        })
                                        .html(`Dark`),
                                    $(`<option>`)
                                        .attr({
                                            value: `rose`
                                        })
                                        .html(`Rosa`),
                                    $(`<option>`)
                                        .attr({
                                            value: `classic`
                                        })
                                        .html(`Klassisch`)
                                )
                        ),
                        $('<div>').append(
                            $('<button>')
                                .attr({
                                    type: 'reset'
                                }).button()
                                .html('Zurücksetzen'),
                            $('<button>')
                                .html('Speichern')
                                .button()
                        )
                    ),
                recommendation
            );

        $('#confGeneral').append(form);
    },
    /**
     * Berechnet die Summe der eingegebenen Empfehlungen und lässt nur Werte zu, die im Gesamten 100% ergeben
     */
    calcRecommSum() {                
        let sum = 0;
        let saveBtn = $('div.ui-dialog-buttonset>button:nth-child(2)');
        saveBtn.button('disable');

        for (const recomm of $('input.recommendation')) {
            sum += $(recomm).val() * 1;
        }
        if (sum < 100) {
            $('#recommErrorMessage').html('Die Summe der Empfehlungen ist zu klein!');
        }
        else if (sum > 100) {
            $('#recommErrorMessage').html('Die Summe der Empfehlungen ist zu groß!');
        }
        else {
            $('#recommErrorMessage').html('');
            saveBtn.button('enable');
        }
        $('#configRecommSum').val(sum);
    }
}