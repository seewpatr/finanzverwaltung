exports.login = {
    /**
     * Erzeugt generisch das Modal zum Einloggen des Users beim Starten der App
     */
    createLoginPanel() {
        $('#mainMenu').addClass(`is-disabled `)

        let loginForm = $(`<div>`)
            .attr({
                id: `login-form`
            })
            .append(
                $(`<ul>`)
                    .append(
                        $(`<li>`)
                            .append(
                                $('<a>')
                                    .attr({
                                        href: '#login'
                                    })
                                    .html(`Einloggen`)
                            ),
                        $(`<li>`)
                            .append(
                                $('<a>')
                                    .attr({
                                        href: '#register'
                                    })
                                    .html(`Registrieren`)
                            )),
                $(`<div>`)
                    .attr(`id`, `login`)
                    .append(
                        $(`<h2>`)
                            .html(`Loggen Sie sich ein`),
                        $(`<hr>`),
                        $(`<form>`)
                            .attr(`id`, `loginForm`)
                            .append(
                                $('<label>')
                                    .attr({
                                        id: 'loginErrorMessage'
                                    }),
                                $('<label>')
                                    .attr({
                                        id: 'loginSuccessMessage'
                                    }),
                                $(`<input>`)
                                    .attr({
                                        id: `loginFormUsername`,
                                        type: `text`,
                                        title: `username`,
                                        placeholder: `Benutzername`,
                                        autocomlete: `off`
                                    }),
                                $('<div>')
                                    .addClass('passwordInput')
                                    .append(
                                        $('<input>')
                                            .attr({
                                                id: 'loginFormPw',
                                                type: 'password',
                                                placeholder: 'Passwort',
                                                title: 'password'
                                            }),
                                        $('<i>')
                                            .addClass('passwordEye passwordEyeClose')
                                            .on('click', this.togglePassword)
                                    ),
                                $(`<p>`)
                                    .addClass(`customLink`)
                                    .on('click', this.createForgotPwForm)
                                    .html(`Passwort vergessen`)
                            )
                    ),
                $(`<div>`)
                    .attr(`id`, `register`)
                    .append(
                        $(`<h2>`)
                            .html(`Legen Sie ein neues Profil an`),
                        $(`<hr>`),
                        $(`<form>`)
                            .attr(`id`, `registerForm`)
                            .append(
                                $('<label>')
                                    .attr({
                                        id: 'registerErrorMessage'
                                    }),
                                $(`<input>`)
                                    .attr({
                                        id: `registerFormUsername`,
                                        type: `email`,
                                        title: `email`,
                                        placeholder: `E-Mail Adresse`
                                    }),
                                $('<div>')
                                    .addClass('passwordInput')
                                    .append(
                                        $('<input>')
                                            .attr({
                                                id: `registerFormPw`,
                                                type: 'password',
                                                placeholder: 'Passwort',
                                                title: 'password'
                                            })
                                            .addClass('form-control'),
                                        $('<i>')
                                            .addClass('passwordEye passwordEyeClose')
                                            .on('click', this.togglePassword)
                                    ),
                                $('<div>')
                                    .addClass('passwordInput')
                                    .append(
                                        $('<input>')
                                            .attr({
                                                id: `registerFormPwRep`,
                                                type: 'password',
                                                placeholder: 'Passwort',
                                                title: 'password'
                                            })
                                            .addClass('form-control'),
                                        $('<i>')
                                            .addClass('passwordEye passwordEyeClose')
                                            .on('click', this.togglePassword)
                                    )
                            )

                    )
            );

        loginForm.appendTo($(`#mainContent`));
        loginForm.tabs();

        // Erzeugung des Modals für Login / Register
        $(`#mainContent`)
            .dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {                                
                    'Einloggen': function () {
                        let lUsername = $('#loginFormUsername');
                        let lPw = $('#loginFormPw');
                        let rUsername = $('#registerFormUsername');
                        let rPw = $('#registerFormPw');
                        let rPwRep = $('#registerFormPwRep');

                        $('#login[aria-hidden=true]').length * 1 == 0
                            ? (
                                this.checkempty(lUsername.val(), lPw.val())
                                    .then(function () {
                                        this.loginUser(lUsername.val(), lPw.val());
                                    }.bind(this))
                                    .catch(function () {
                                        $('#loginErrorMessage').html('Die Eingaben dürfen nicht leer sein!');
                                    })
                            )
                            : (
                                this.checkempty(rUsername.val(), rPw.val(), rPwRep.val())
                                    .then(function () {
                                        this.validatePassword(rPw.val(), rPwRep.val())
                                            .then(function () {
                                                this.registerUser(rUsername.val(), rPw.val());
                                                // $('#mainContent').dialog("close");
                                                // $(`#mainMenu`).toggleClass(`is-disabled`);
                                            }.bind(this))
                                            .catch(function () {
                                                $('#registerErrorMessage').html('Die eingegebenen Passwörter müssen übereinstimmen!')
                                            })
                                    })
                                    .catch(function () {
                                        $('#registerErrorMessage').html('Die Eingaben dürfen nicht leer sein!')
                                    })
                            )
                    }.bind(this)
                },
                autoOpen: true,
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                }
            });

        // TODO Enter Button abfangen
        // $('#mainContent').live('keyup', function(event) {
        //     if(event.keyCode == 13) {
        //         $(':button:contains("Einloggen")').click();  
        //     }
        // });

        // Erzeugung des Modals für PW forgot
        let pwResetBtn = $('<button>')
            .html('Zurücksetzen')
            .addClass('ui-button ui-corner-all ui-widget disableForPwForgot')
            .attr({
                id: 'pwForgotBtn'
            })
            .on('click', function () {
                let username = $('#PwForGotUsername');
                this.checkempty(username.val())
                    .then(function () {
                        // TODO PW Zurücksetzen funktion bauen
                        $('#login').toggleClass('disableForPwForgot');
                        $('#pwForgotDiv').toggleClass('disableForPwForgot');
                        $('li[tabindex=-1]').toggleClass('disableForPwForgot');
                        $('li[tabindex=0]>a').html('Einloggen');                              
                        $('div.ui-dialog-buttonset >button:first-child').toggleClass('disableForPwForgot');
                        $('#pwForgotBtn').toggleClass('disableForPwForgot');
                        $('#loginSuccessMessage').html('Es wurde ein E-Mail mit den weiteren Schritten verschickt');           
                        this.resetPw(username.val());
                        username.val('');
                    }.bind(this))
                    .catch(function () {
                        $('#loginErrorMessage').html('Die Eingabe darf nicht leer sein!')
                    });
            }.bind(this));

        $('div.ui-dialog-buttonset').append(pwResetBtn);
    },
    /**
     * Erzeugt den Input und Button für das PW - Reset
     */
    createForgotPwForm() {
        // TODO eigenes DIV bauen und einblenden
        $('#login').toggleClass('disableForPwForgot');
        $('li[tabindex=-1]').toggleClass('disableForPwForgot');
        $('li[tabindex=0]>a').html('Zurücksetzen');
        $('div.ui-dialog-buttonset >button:first-child').toggleClass('disableForPwForgot');
        $('#pwForgotBtn').toggleClass('disableForPwForgot');

        $('<div>')
            .addClass('ui-tabs-panel')
            .attr({
                id: 'pwForgotDiv'
            })
            .append(
                $('<h2>').html('Setzen Sie Ihr Passwort zurück'),
                $('<hr>'),
                $('<form>')
                    .attr({
                        id: 'pwForgotForm'
                    })
                    .append(
                        $('<label>')
                            .attr({
                                id: 'pwForgotErrorMessage'
                            }),
                        $('<input>')
                            .attr({
                                id: 'pwForgotUsername',
                                type: 'text',
                                placeholder: 'Username',
                                autocomlete: 'off'
                            }),
                        $('<p>')
                            .addClass('customLink')
                            .on('click', () => {
                                $('#login').toggleClass('disableForPwForgot');
                                $('#pwForgotDiv').toggleClass('disableForPwForgot');
                                $('li[tabindex=-1]').toggleClass('disableForPwForgot');
                                $('li[tabindex=0]>a').html('Einloggen');                              
                                $('div.ui-dialog-buttonset >button:first-child').toggleClass('disableForPwForgot');
                                $('#pwForgotBtn').toggleClass('disableForPwForgot');
                            })
                            .html('Zurück zum Login')
                    )
            ).insertAfter('ul[role="tablist"]');
    },
    /**
     * Switcht die Sichtbarkeit der Passworteingaben
     */
    togglePassword() {
        $(this).toggleClass('passwordEyeClose').toggleClass('passwordEyeOpen');
        $(this).prev().attr('type') === 'password' ? $(this).prev().attr({ type: 'text' }) : $(this).prev().attr({ type: 'password' });
    },
    /**
     * Leert alle Statusmeldungen 
     */
    emptyStatusMessages() {
        $('input').on('focus', function () {
            $('#loginErrorMessage').html('');
            $('#registerErrorMessage').html('');
            $('#loginSuccessMessage').html('');
        });
    },
    /**
     * Prüft ob übergebene Werte leer sind und liefert ein Promise zurück
     */
    checkempty() {
        return new Promise((resolve, reject) => {
            let empty = false;
            for (const value of arguments) {
                if (value == '') {
                    empty = true;
                }
            }
            (empty)
                ? reject()
                : resolve();
        });
    },
    /**
     * Validiert ein Passwort anhand doppelter Eingabe
     */
    validatePassword() {
        return new Promise((resolve, reject) => {
            if (arguments[0] === arguments[1]) {
                console.log('PW - Validierung erfolgreich');
                resolve();
            }
            else {
                console.log('PW - Validierung fehlgeschlagen');
                reject();
            }
        });
    },
    /**
     * Sendet einen Login-Request mit Username & Passwort an den Server
     * @param {string} username Erwartet einen Usernamen vom Datentyp String
     * @param {string} password Erwartet ein Passwort vom Datentyp String
     */
    loginUser(username, password) {
        console.log('login')
        let user = { username: username, password: password };

        $.ajax({
            // url: 'http://84.16.242.168:8287/finance/loginUser/',
            url: 'http://localhost:8287/finance/loginUser/',
            method: 'post',
            data: user,
            dataType: 'json',
            success: (response) => {
                if (!$.isEmptyObject(response.data)) {
                    $('#mainContent').dialog("close");
                    $(`#mainMenu`).toggleClass(`is-disabled`);
                    navigation.setSessions(response.data);
                    ipcRenderer.sendSync('beginSession', response.data);
                }
                else {
                    $('#loginFormUsername').focus();
                    $('#loginErrorMessage').html(response.status.message);
                }
            }
        });
    },
    /**
     * Sendet einen Register-Request mit E-Mail & Passwort an den Server
     * @param {string} email Erwartet eine E-Mail Adresse vom Datentyp String
     * @param {string} password Erwartet ein Passwort vom Datentyp String
     */
    registerUser(email, password) {

        ipcRenderer.send('registerUser', { email: email, password: password });

        ipcRenderer.on('registerSuccess', function (event, data) {
            console.log('Registrierung erfolgreich');
            $('#login-form').tabs({ active: 0 });
            $('#loginSuccessMessage').html(`${data.status.message}\nEine E-Mail mit den weiteren Schritten wurde an ${data.data} geschickt`);

        });
        ipcRenderer.on('error', function (event, data) {
            $('#registerFormUsername').focus();
            $('#registerErrorMessage').html(data.message);
        });
    },
    /**
     * Setzt einen PW-Reset Request an den Server
     * @param {string} username Erwartet einen Usernamen vom Datentyp String
     */
    resetPw(username) {
        console.log('Passwort angefordert');
        console.log(username);

    }
}