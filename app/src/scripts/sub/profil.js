exports.profil = {
    accountTypes: navigation.getSession('accountTypes'),
    currentUser: navigation.getSession('currentUser'),
    currentUserAccounts: navigation.getSession('currentUserAccounts'),

    // TODO Augen ausrichten
    // TODO PW Ändern einbauen

    /**
     * Ruft Methoden zur Erzeugung des gesamten Contents für den Reiter Profil auf  
     */
    createProfilContent() {
        this.createProfileGeneralContent();
        this.createProfileUpdateContent();
        this.createProfileDeleteContent();
        this.createProfileAccountsContent();
        this.toggleSorting();
        // this.accountTypes = ;
        // this.currentUser = ;
        // this.currentUserAccounts = ;
    },

    /**
     * Erzeugt generisch den Inhalt für den Reiter Profil - Allgemein 
     */
    createProfileGeneralContent() {
        
        let form = $('<form>')
            .append(
                $('<div>').append(
                    $('<label>')
                        .html(`ID: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.currentUser.id}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Rolle: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.role)}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Geschlecht: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: this.currentUser.gender == 1 ? 'Männlich' : 'Weiblich'
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Vorname: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.firstname)}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Nachname: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.lastname)}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`E-Mail Adresse: `),
                    $('<input>')
                        .attr({
                            type: 'email',
                            disabled: 'disabled',
                            value: `${this.currentUser.email}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Notizen: `),
                    $('<input>')
                        .attr({
                            type: 'email',
                            disabled: 'disabled',
                            value: `${this.currentUser.comments}`
                        }))
            );

        $('#profGeneral').append(form);

    },
    /**
     * Erzeugt generisch den Inhalt für den Reiter Profil - Edit
     */
    createProfileUpdateContent() {
        let form = $('<form>')
            .attr({
                id: 'updateProfile'
            })
            .append(
                $('<div>').append(
                    $('<label>')
                        .html(`ID: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.currentUser.id}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Rolle: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.role)}`
                        })
                ),
                $('<div>')

                    .append(
                        $('<label>'),
                        $('<span>')
                            .attr({
                                id: 'update-profileErrorMessage'
                            })
                    ),
                $('<div>').append(
                    $('<label>')
                        .html(`Geschlecht: `),
                    $(`<select>`)
                        .attr(`data-type`, `update-profileGender`)
                        .append(
                            $(`<option>`)
                                .attr({
                                    value: `type`,
                                    disabled: `disabled`
                                })
                                .html(`Geschlecht`),
                            $('<option>')
                                .attr({
                                    value: 2,
                                })
                                .html('Neutral'),
                            $('<option>')
                                .attr({
                                    value: 1,
                                })
                                .html('Männlich'),
                            $('<option>')
                                .attr({
                                    value: 0,
                                })
                                .html('Weiblich')
                        )
                ),
                $('<div>').append(
                    $('<label>')
                        .html(`Vorname: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profileFirstname',
                            type: 'text',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.firstname)}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Nachname: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profileLastname',
                            type: 'text',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.lastname)}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`E-Mail Adresse: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profileEmail',
                            type: 'email',
                            value: this.currentUser.email
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Notizen: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profileComments',
                            type: 'text',
                            value: this.currentUser.comments
                        })),
                $('<div>').append(
                    $('<label>')
                        .html(`Passwort: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profilePw',
                            type: 'password'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye passwordEyeClose')
                        .on('click', this.togglePassword)
                ),
                $('<div>').append(
                    $('<label>')
                        .html(`Passwort wiederholen: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profilePwRep',
                            type: 'password'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye eye2 passwordEyeClose')
                        .on('click', this.togglePassword),
                    $('<button>')
                        .attr({
                            type: 'submit',
                            id: 'changeUserPwBtn'
                        })
                        .on('click', this.createPwChangeDialog)
                        .button()
                        .html(''),
                ),
                $('<div>').append(
                    $('<button>')
                        .attr({
                            type: 'reset'
                        })
                        .on('click', this.resetForm)
                        .button()
                        .html('Zurücksetzen'),
                    $('<button>')
                        .html('Speichern')
                        .on('click', function (e) {
                            e.preventDefault();
                            let pw = $('input[data-type="update-profilePw"]').val();
                            let pwRep = $('input[data-type="update-profilePwRep"]').val();
                            this.checkEmpty(pw, pwRep)
                                .then(function () {
                                    this.validatePassword(pw, pwRep)
                                        .then(this.updateUsersProfile)
                                        .catch(function () {
                                            $('#update-profileErrorMessage').html('Die eingegebenen Passwörter müssen übereinstimmen!')
                                        });
                                }.bind(this))
                                .catch(function () {
                                    $('#update-profileErrorMessage').html('Die Eingaben dürfen nicht leer sein!');
                                });
                        }.bind(this))
                        .button()
                )
            );

        $('#profEdit').append(form);
        // Wählt das richtige Geschlecht bei der Dropdownliste aus
        $(`select[data-type="update-profileGender"] >option[value=${this.currentUser.gender}]`).attr({
            selected: 'selected'
        });
    },
    /**
     * Erzeugt generisch den Inhalt für den Reiter Profil - Delete
     */
    createProfileDeleteContent() {        
        let form = $('<form>')
            .append(
                $('<div>').append(
                    $('<label>')
                        .attr({
                            for: `profileEditId`
                        })
                        .html(`ID: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.currentUser.id}`
                        })),
                $('<div>').append(
                    $('<label>')
                        .attr({
                            for: `profileEditRole`
                        })
                        .html(`Rolle: `),
                    $('<input>')
                        .attr({
                            type: 'text',
                            disabled: 'disabled',
                            value: `${this.formatFirstLetterToUpperCase(this.currentUser.role)}`
                        })
                        .val(`Benutzer`)),
                $('<div>').append(
                    $('<label>')
                        .attr({
                            for: `profileDeletePw`
                        })
                        .html(`Passwort: `),
                    $('<input>')
                        .attr({
                            type: 'password',
                            id: 'profileDeletePw'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye eye3 passwordEyeClose')
                        .on('click', this.togglePassword)
                ),
                $('<div>').append(
                    $('<label>')
                        .attr({
                            for: `profileDeletePwRep`
                        })
                        .html(`Passwort wiederholen: `),
                    $('<input>')
                        .attr({
                            type: 'password',
                            id: 'passwordDeletePwRep'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye eye4 passwordEyeClose')
                        .on('click', this.togglePassword)
                ),
                $('<div>').append(
                    $('<button>')
                        .attr({
                            type: 'reset'
                        })
                        .on('click', function (e) {
                            e.preventDefault();
                            let entries = $('#profDelete>form>div');
                            this.resetForm(entries);
                        }.bind(this))
                        .button()
                        .html('Zurücksetzen'),
                    // TODO Speichert später die Werte und schickt sie an den Server (Profil deaktiviert)   
                    $('<button>')
                        .html('Löschen')
                        .button()
                )
            );

        $('#profDelete').append(form);
    },
    /**
     * Erzeugt generisch den Inhalt für den Reiter Profil - Accounts
     */
    createProfileAccountsContent() {
        let table = $(`<table>`)
            .attr({
                id: `createAccountsTable`
            })
            .append($(`<thead>`)
                .append($(`<tr>`)
                    .append(
                        $(`<th>`)
                            .addClass(`alignCenter sort`)
                            .html(`#`),
                        $(`<th>`)
                            .addClass(`alignLeft  sort`)
                            .html(`Kontotyp`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`Bezeichnung`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`IBAN`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`BIC`),
                        $(`<th>`)
                            .addClass(`alignLeft sort`)
                            .html(`Akt. Stand`),
                        $(`<th>`)
                            .addClass(`alignCenter`)
                            .html(`Kommentar`),
                        $(`<th>`)
                            .addClass(`alignCenter`),
                        $(`<th>`)
                            .addClass(`alignCenter`)
                    )
                )
            ).appendTo(`#profAccounts`);
        $(`<tbody>`).appendTo(table)
        this.getcurrentUserAccounts();
    },
    /**
     * Erzeugt einen Dialog zur Editierung eines bestehenden Passworts
     */
    createPwChangeDialog(e) {
        e.preventDefault();
        // TODO Funktionalität einbauen
        let changePw = $('<div>')
            .attr({
                id: 'changePwDialog'
            })
            .append(
                $('<div>').append(
                    $('<label>')
                        .html(`Altes Passwort: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profilePw',
                            type: 'password'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye passwordEyeClose')
                        .on('click', this.togglePassword)
                ),
                $('<div>').append(
                    $('<label>')
                        .html(`Neues Passwort: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profilePw',
                            type: 'password'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye passwordEyeClose')
                        .on('click', this.togglePassword)),
                $('<div>').append(
                    $('<label>')
                        .html(`Neues Passwort wiederholen: `),
                    $('<input>')
                        .attr({
                            'data-type': 'update-profilePwRep',
                            type: 'password'
                        })
                        .addClass('passwordInput'),
                    $('<i>')
                        .addClass('passwordEye eye2 passwordEyeClose')
                        .on('click', this.togglePassword)
                )
            );

        changePw.appendTo('#profGeneral');

        // Erzeugt das Modal zum Bearbeiten der Konto-Einträge
        changePw
            .dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 'auto',
                modal: true,
                title: 'Passwort ändern',
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                    },
                    'Speichern': function () {
                        _this.createOrUpdateAccount('update')
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            });
    },
    /**
     * Erzeugt einen Dialog zur Editierung eines bestehenden Accounts
     * @param {object} account Erwartet ein Object vom Datentyp Account, das hier editiert werden soll
     */
    createUpdateAccountDialog(account) {
        let _this = this;

        let updateAccount = $('<div>')
            .append(
                $('<table>')
                    .attr({
                        id: 'updateAccountTable'
                    })
                    .append(
                        $('<thead>')
                            .append($(`<tr>`)
                                .append(
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`#`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`Kontotyp`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`Bezeichnung`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`IBAN`),
                                    $(`<th>`)
                                        .addClass(`alignLeft`)
                                        .html(`BIC`),
                                    $(`<th>`)
                                        .addClass(`alignRight`)
                                        .html(`Akt. Stand`),
                                    $(`<th>`)
                                        .addClass(`alignCenter`)
                                        .html(`Kommentar`)
                                )
                            ),
                        $('<tbody>')
                            .append(
                                $(`<tr>`)
                                    .append(
                                        $(`<td>`)
                                            .addClass(`alignCenter`)
                                            .html(`${account.id}`)
                                            .attr(`data-type`, `update-accountId`),
                                        $(`<td>`)
                                            .addClass(`alignLeft`)
                                            .append(
                                                $(`<select>`)
                                                    .attr(`data-type`, `update-accountType`)
                                                    .append(
                                                        $(`<option>`)
                                                            .attr({
                                                                value: `type`,
                                                                disabled: `disabled`
                                                            })
                                                            .html(`Kontotyp`)
                                                    )
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignLeft`)
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'text',
                                                        value: `${account.name}`,
                                                        'data-type': 'update-accountName'
                                                    })
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignLeft`)
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'text',
                                                        value: `${account.iban}`,
                                                        'data-type': 'update-accountIban'
                                                    })
                                            ),
                                        $(`<td>`)
                                            .addClass(`alignLeft`)
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'text',
                                                        value: `${account.bic}`,
                                                        'data-type': 'update-accountBic'
                                                    })
                                            ),
                                        $(`<td>`)
                                            .addClass('alignRight')
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'number',
                                                        value: `${account.balance}`,
                                                        'data-type': 'update-accountBalance'
                                                    })
                                            ),
                                        $(`<td>`)
                                            .addClass('alignCenter')
                                            .append(
                                                $('<input>')
                                                    .attr({
                                                        type: 'textarea',
                                                        value: `${account.comments}`,
                                                        'data-type': 'update-accountComments'
                                                    })
                                            )
                                    )
                            )
                    )
            );

        updateAccount.appendTo('#profAccounts');

        // Erzeugt das Modal zum Bearbeiten der Konto-Einträge
        updateAccount
            .dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 'auto',
                modal: true,
                title: 'Eintrag bearbeiten',
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                    },
                    'Speichern': function () {
                        _this.createOrUpdateAccount('update')
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            });

        for (const type of this.accountTypes) {
            let option = $(`<option>`)
                .attr({
                    value: `${type.id}`
                })
                .html(`${type.name}`);

            if (type.id == account.accounttype_id) {
                option.attr({
                    selected: 'selected'
                });
            };
            option.appendTo('#updateAccountTable select[data-type="update-accountType"]');
        }
    },

    /**
     * Befüllt die Contentabelle mit allen Konten des aktuell eingeloggten Users
     */
    getcurrentUserAccounts() {
        let _this = this;
        $('#createAccountsTable >tbody').children().remove();

        for (const account of this.currentUserAccounts) {
            let currentAccount = account;
            $(`<tr>`)
                .append($(`<td>`)
                    .addClass(`alignCenter`)
                    .html(`${account.id}`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`${this.accountTypes[account.accounttype_id - 1].name}`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`${account.name}`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`${account.iban}`),
                    $(`<td>`)
                        .addClass(`alignLeft`)
                        .html(`${account.bic}`),
                    $(`<td>`)
                        .addClass(`alignRight`)
                        .html(`${account.balance}`),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .html(`${account.comments}`), $(`<td>`)
                            .addClass(`alignCenter`)
                            .append($(`<button>`)
                                .addClass('iconBtn editBtn')
                                .on('click', function () {
                                    this.createUpdateAccountDialog(currentAccount);
                                }.bind(this))
                            ),
                    $(`<td>`)
                        .addClass(`alignCenter`)
                        .append(
                            $(`<button>`)
                                .data('id', account.id)
                                .addClass('iconBtn deleteBtn')
                                .on('click', function () {
                                    let id = $(this).data('id');
                                    _this.checkEmpty(id)
                                        .then(function () {
                                            _this.createDeleteAccountDialog(id);
                                        })
                                        // TODO Kann dieses Catch jemals eintreten?
                                        .catch(function () {
                                            console.log('Fehler')
                                        });
                                })
                        )
                )
                .appendTo('#createAccountsTable >tbody');
        }

        $(`<tr>`)
            .append(
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .html(`${this.currentUserAccounts.length + 1}`)
                    .attr('data-type', 'create-accountid'),
                $(`<td>`)
                    .addClass(`alignLeft`)
                    .append(
                        $(`<select>`)
                            .attr(`data-type`, `create-accountType`)
                            .append(
                                $(`<option>`)
                                    .attr({
                                        value: `type`,
                                        selected: `selected`,
                                        disabled: `disabled`
                                    })
                                    .html(`Kontotyp`)
                            )
                    ),
                $(`<td>`)
                    .addClass(`alignLeft`)
                    .append(
                        $('<input>')
                            .attr({
                                type: 'text',
                                'data-type': 'create-accountName',
                                placeholder: 'Bezeichnung (optional)'
                            })
                    ),
                $(`<td>`)
                    .addClass(`alignLeft`)
                    .append(
                        $('<input>')
                            .attr({
                                type: 'text',
                                'data-type': 'create-accountIban',
                                placeholder: 'IBAN (nicht bei Barkonto)'
                            })
                    ),
                $(`<td>`)
                    .addClass(`alignLeft`)
                    .append(
                        $('<input>')
                            .attr({
                                type: 'text',
                                'data-type': 'create-accountBic',
                                placeholder: 'BIC (nicht bei Barkonto)'
                            })
                    ),
                $(`<td>`)
                    .addClass(`alignRight`)
                    .append($('<input>')
                        .attr({
                            type: 'number',
                            placeholder: 'Akt. Stand (optional)',
                            'data-type': 'create-accountBalance'
                        })
                    ),
                $(`<td>`)
                    .addClass('alignCenter')
                    .append($('<input>')
                        .attr({
                            placeholder: `Kein Kommentar`,
                            'data-type': 'create-accountComments'
                        })
                    ),
                $(`<td>`),
                $(`<td>`)
                    .addClass(`alignCenter`)
                    .append($(`<button>`)
                        .addClass('iconBtn addBtn')
                        .on('click', function (event) {
                            event.preventDefault();
                            this.createOrUpdateAccount('create');
                        }.bind(this)))).appendTo('#createAccountsTable >tbody');

        for (const type of this.accountTypes) {
            $(`<option>`)
                .attr({
                    value: `${type.id}`
                })
                .html(`${type.name}`)
                .appendTo('select[data-type="create-accountType"]');
        }
    },
    /**
    * Löscht alle Felder des übergebenen Arrays, außer Benutzer_Id & Rolle    
    */
    resetForm() {
        $(`option[value=${this.currentUser.gender}]`).prop({
            selected: 'selected'
        });
        $('input[data-type="update-profileFirstname"]').val(`${this.currentUser.firstname}`);
        $('input[data-type="update-profileLastname"]').val(`${this.currentUser.lastname}`);
        $('input[data-type="update-profileEmail"]').val(`${this.currentUser.email}`);
        $('input[data-type="update-profileComments"]').val(`${this.currentUser.comments}`);
        $('input[data-type="update-profilePw"]').val(``);
        $('input[data-type="update-profilePwRep"]').val(``);
    },

    /**
      * Editiert und speichert das aktuell eingeloggte Userprofil 
     */
    updateUsersProfile(e) {
        e.preventDefault();
        let user = {
            id: this.currentUser.id,
            gender: $(`select[data-type="update-profileGender"]`).val(),
            firstname: $('input[data-type="update-profileFirstname"]').val(),
            lastname: $('input[data-type="update-profileLastname"]').val(),
            comments: $('input[data-type="update-profileEmail"]').val(),
            password: $('input[data-type="update-profilePw"]').val()
        }

        ipcRenderer.send('updateUser', user);
        ipcRenderer.on('updateUserSuccess', function (event, user) {
            console.log(user.status);
            navigation.setSessions(user.data);
            this.getSessions();
            location.reload();

        }.bind(this));

        // TODO Fehlerbehandlung
        ipcRenderer.on('error', function (event, user) {
            alert('fehler');
        });
    },
    /**
     * Switcht zwischen aufsteigender und absteigender Reihenfolge
     */
    toggleSorting() {
        // TODO wird später noch auf echte Sortierung umgebaut
        $('th.sort').on('click', function () {
            $(this).toggleClass('sortDesc');
        });
    },

    // TODO Erneut bauen
    // /**
    //  * Switcht die Sichtbarkeit der Passworteingaben
    //  */
    // togglePassword() {
    //     return function () {
    //         $(this).toggleClass('passwordEyeClose').toggleClass('passwordEyeOpen');
    //         $(this).prev().attr('type') === 'password' ? $(this).prev().attr({ type: 'text' }) : $(this).prev().attr({ type: 'password' });
    //     };
    // },

    /**
     * Formatiert den ersten Buchstaben eines Strings zu einem Großbuchstaben
     * @param {string} text Erwartet einen Text vom Datentyp String, der entsprechend formatiert werden soll
     */
    formatFirstLetterToUpperCase(text) {        
        let formattedText = text.toLowerCase();
        return formattedText.charAt(0).toUpperCase() + text.slice(1);
    },
    /**
     * Liest je nach übergebener Methode entweder das Formular für Update oder Create aus. 
     * Mit den Werten wird dann entweder ein neuer Account angelegt oder ein bestehender aktualisiert
     * @param {string} method Erwartet als Keyword entweder 'update' oder 'create' vom Datentyp String
     */
    createOrUpdateAccount(method) {
        let accounttype = $(`#${method}AccountsTable select[data-type="${method}-accountType"]`).val();
        let balance = $(`#${method}AccountsTable input[data-type="${method}-accountBalance"]`).val();
        let id = $(`#${method}AccountsTable td[data-type="${method}-accountId"]`).html();
        let bic = $(`#${method}AccountsTable input[data-type="${method}-accountBic"]`).val();
        let comments = $(`#${method}AccountsTable input[data-type="${method}-accountComments"]`).val();
        let iban = $(`#${method}AccountsTable input[data-type="${method}-accountIban"]`).val();
        let name = $(`#${method}AccountsTable input[data-type="${method}-accountName"]`).val();

        this.checkEmpty(accounttype, id)
            .then(function () {
                account = {
                    accounttype_id: accounttype * 1,
                    balance: balance * 1,
                    bic: bic,
                    comments: comments,
                    iban: iban,
                    id: id * 1,
                    name: name,
                    user_id: this.currentUser.id
                }

                ipcRenderer.send('createOrUpdateAccount', account);
                ipcRenderer.on('accountCreatedOrUpdated', function (event, account) {
                    console.log(account.status);
                    navigation.setSessions();
                    this.getSessions();
                    this.getcurrentUserAccounts();
                }.bind(this));

                ipcRenderer.on('error', function (event, account) {
                    // TODO Ist dieses Catch aufrufbar???
                    alert('Fehler');
                });
            }.bind(this))
            .catch(function () {
                $(`#${method}AccountsTable select[data-type="${method}-accountType"]`).attr({
                    title: 'Bitte wählen Sie einen Kontotyp aus'
                }).tooltip()
                $(`#${method}AccountsTable select[data-type="${method}-accountType"]`).tooltip('open')
            });
    },

    /**
     * Erzeugt einen Dialog zur erneuten Abfrage der Löschung eines gewählten Accounts
     * @param {integer} id 
     */
    createDeleteAccountDialog(id) {
        let _this = this;
        let deleteAccount =
            $('<div>')
                .attr({
                    id: 'deleteAccount'
                })
                .append('<h1>').html(`Möchten Sie den Account mit der <span class="deleteAccountId">ID ${id}</span> wirklich löschen?`);

        deleteAccount.appendTo('#profAccounts');

        deleteAccount
            .dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 'auto',
                modal: true,
                title: 'Eintrag bearbeiten',
                buttons: {
                    'Abbrechen': function () {
                        $(this).dialog("close");
                    },
                    'Löschen': function () {
                        _this.deleteAccount(id)
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "fade",
                    duration: 500
                },
                hide: {
                    effect: "fade",
                    duration: 500
                }
            });
    },
    /**
     * Löscht einen existierenden Account anhand der übergebenen ID
     * @param {integer} id Erwartet die ID vom Datentyp Integer eines existierenden Accounts 
     */
    deleteAccount(id) {
        ipcRenderer.send('deleteAccount', id);

        ipcRenderer.on('accountDeleted', function (event, account) {
            console.log(account.status);
            navigation.setSessions();
            this.getSessions();
            this.getcurrentUserAccounts();
        }.bind(this));
    },

    /**
    * Prüft ob übergebene Werte leer sind und liefert ein Promise zurück
    */
    checkEmpty() {
        return new Promise((resolve, reject) => {
            let empty = false;
            for (const value of arguments) {
                if (value == '' || value == null) {
                    empty = true;
                }
            }
            (empty)
                ? reject()
                : resolve();
        });
    },
    /**
   * Validiert ein Passwort anhand doppelter Eingabe
   */
    validatePassword() {
        return new Promise((resolve, reject) => {
            if (arguments[0] === arguments[1]) {
                console.log('PW - Validierung erfolgreich');
                resolve();
            }
            else {
                console.log('PW - Validierung fehlgeschlagen');
                reject();
            }
        });
    },
}
