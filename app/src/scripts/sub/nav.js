exports.navigation = {
    /**
     * Erzeugt die Technik hinter der Navigation des Haupt-Menüs
     * @param {*} currentEntry Erwartet den aktuell geöffneten Eintrag vom Datentyp string
     */
    createMainNavigation(currentEntry) {        
        let mainMenu = new Navigation().mainMenu;
        let mainWrapper = $('#mainMenu');
        let icons = {
            "header": "ui-icon-triangle-1-e",
            "activeHeader": "ui-icon-triangle-1-s"
        };

        for (const entry of mainMenu) {
            let mainEntry = $('<div>')
                .addClass('group')
                .append(
                    $('<h3>').html(entry.title)
                );
            let sub = $('<ul>').addClass('subgroup').appendTo(mainEntry);

            for (let index = 0; index < entry.subEntries.length; index++) {
                let subEntry = $('<li>')
                    .append(
                        $('<a>')
                            .html(entry.subEntries[index].title)
                            .data('index', index)
                            .attr({
                                href: entry.subEntries[index].href
                            })
                            .on('click', function () {
                                let i = $(this).data('index');
                                $(`ul[role="tablist"]`).parent().tabs({ active: i })
                            })
                    );
                subEntry.appendTo(sub);

            }
            mainEntry.appendTo(mainWrapper);
        }

        mainWrapper.accordion({
            collapsible: true,
            active: currentEntry,
            heightStyle: "content",
            icons: icons,
            header: " h3",
            animate: 1000
        })
            // TODO Sortable not fixed    
            .sortable({
                axis: "y",
                handle: "h3",
                stop: function (event, ui) {
                    ui.item.children("h3").triggerHandler("focusout");


                    // Refresh accordion to handle new order
                    $(this).accordion("refresh");
                }
            });

        $('div>h3[aria-expanded="true"]').parent().toggleClass('customActive');
        // TODO Bessere lösung finden für Hilfe Style
        $($('div.group')[5]).removeClass('customActive');

    },
    /**
     * Erzeugt die Technik hinter der Navigation des Haupt-Contents
     */
    createTabNavigation() {
        let tabMenu = new TabNavigation();
        let area;

        for (const menu of tabMenu.tabList) {
            let ul = $('<ul>');

            for (const tabEntry of menu) {
                area = tabEntry.area;
                let tab = $('<li>')
                    .append(
                        $('<a>')
                            .attr({
                                href: `#${tabEntry.href}`
                            })
                            .html(`${tabEntry.title}`)
                    );

                let tabContent = $('<div>')
                    .attr({
                        id: `${tabEntry.href}`
                    });

                ul.append(tab);
                $(`#${area}MainContent`).append(tabContent);
            }
            $(`#${area}MainContent`).prepend(ul);
            $(`#${area}MainContent`).tabs({
                disabled: [3],
                // TODO Scroll to Top aktivieren
                // activate: function (event, ui) {
                //     $('html').scrollTop();
                // }
            });
        }
    },
    /**
     * Setzt alle notwendigen SessionStorages, die für die Applikation benötigt werden
     * @param {object} user Erwartet einen User vom Datentyp Object
     */
    setSessions(user) {
        // let user = ipcRenderer.sendSync('getUserById', userId);
        
        $.ajax({
            // url: `http://84.16.242.168:8287/finance/user/${user.id}`,
            url: `http://localhost:8287/finance/user/${user.id}`,
            method: 'get',
            data: {},
            dataType: 'json',
            success: (response) => {
                console.log(response);
                sessionStorage.setItem('currentUser', JSON.stringify(response.data));
                sessionStorage.setItem('currentUserAccounts', JSON.stringify(response.accountsData));
            }
        }); 

        $.ajax({
            // url: 'http://84.16.242.168:8287/finance/accounttypes/',
            url: 'http://localhost:8287/finance/accounttypes/',
            method: 'get',
            data: {},
            dataType: 'json',
            success: (response) => {
                console.log(response);
                sessionStorage.setItem('accountTypes', JSON.stringify(response.data));
            }
        }); 
    },
      /**
     * Ruft die aktuell vorhandenen SessionStorages auf und speichert diese global für das aktuelle Modul
     */
    getSession(item) {        
       return JSON.parse(sessionStorage.getItem(item));      
    }
}

/**
 * Dies ist eine Basisklasse zur Erzeugung eines Menüs samt Verschachtelung. Dabei sind die @subEntries eine Ansammlung untergeordneter Menüpunkte
 * @title Erwartet den Titel des Menüpunktes vom Datentyp String
 * @href Erwartet den Link des Menüpunktes vom Datentyp String
 * @subEntries Erwartet ein Array an Sub-Menüs
 * @cssClasses Erwartet eine Sammlung von CSS-Klassen vom Datentyp String
 */
class Menu {
    constructor(title, href, subEntries, cssClasses, id = 0) {
        this.id = id;
        this.title = title;
        this.href = href;
        this.subEntries = subEntries;
        this.cssClasses = cssClasses;
    }
}

/**
 * Dies ist eine Klasse, die einen einzelnen Tab - Eintrag des Haupt-Contents darstellt
 */
class TabEntry {
    constructor(title, href = '#', area) {
        this.title = title;
        this.href = href;
        this.area = area;
    }
}

/**
 * Dies ist eine Klasse, die die Struktur der Tab-Navigation des Haupt-Contents darstellt
 */
class TabNavigation {
    constructor() {
        this.tabList = [
            [
                new TabEntry(`Allgemein`, `profGeneral`, `profil`),
                new TabEntry(`Editieren`, `profEdit`, `profil`),
                new TabEntry(`Konten`, `profAccounts`, `profil`),
                new TabEntry(`Profil löschen`, `profDelete`, `profil`)
            ],
            [
                new TabEntry(`Allgemein`, `finGeneral`, `finanzen`),
                new TabEntry(`Details`, `finDetails`, `finanzen`),
                new TabEntry(`Kalender`, `finCalendar`, `finanzen`)
            ],
            [
                new TabEntry(`Allgemein`, `confGeneral`, `konfiguration`),
                new TabEntry(`Anzeige`, `confDisplay`, `konfiguration`),
                new TabEntry(`Einstellungen`, `confSettings`, `konfiguration`)
            ]
        ]
    }
}

/**
 * Dies ist eine übergeordnete Klasse die die gesamte Navigation der Applikation beinhaltet
 * Dies sind die Menüs als auch die Login- und Registrierungsformulare
 */
class Navigation {
    constructor() {
        this.mainMenu = [
            new Menu(`Profil`, `#`, [
                new Menu(`Übersicht`, `profil.html#profGeneral`, [], ``),
                new Menu(`Editieren`, `profil.html#profEdit`, [], ``),
                new Menu(`Konten`, `profil.html#profAccounts`, [], `has-sub`),
                new Menu(`Profil löschen`, `profil.html#profDelete`, [], `last`)
            ], `has-sub`, 1),
            new Menu(`Konfiguration`, `#`, [
                new Menu(`Allgemein`, `konfiguration.html#confGeneral`, [], ``),
                new Menu(`Anzeige`, `konfiguration.html#confDisplay`, [], ``),
                new Menu(`Einstellungen`, `konfiguration.html#confSettings`, [], `last`)
            ], `has-sub`, 2),
            new Menu(`Finanzen`, `#`, [
                new Menu(`Allgemein`, `finanzen.html#finGeneral`, [], ``),
                new Menu(`Detailliste`, `finanzen.html#finDetails`, [], `has-sub`),
                new Menu(`Kalender`, `#`, [], `last`)
            ], `has-sub`, 3),
            new Menu(`Statistik`, `#`, [
                new Menu(`Allgemein`, `#`, [], ``),
                new Menu(`Diagramme`, `#`, [
                    new Menu(`Torte`, `#`, [], ``),
                    new Menu(`Balken`, `#`, [], ``),
                    new Menu(`Graph`, `#`, [], `last`)
                ], `has-sub`)
            ], `has-sub`, 4),
            new Menu(`Datentransfer`, `#`, [
                new Menu(`Import`, `#`, [
                    new Menu(`CSV - Dateien`, `#`, [], ``),
                    new Menu(`XML - Dateien`, `#`, [], ``),
                    new Menu(`Excel - Dateien`, `#`, [], `last`)
                ], `has-sub`),
                new Menu(`Export`, `#`, [
                    new Menu(`CSV - Dateien`, `#`, [], ``),
                    new Menu(`XML - Dateien`, `#`, [], ``),
                    new Menu(`Excel - Dateien`, `#`, [], ``),
                    new Menu(`PDF - Dateien`, `#`, [], `last`)
                ], `has-sub`),
            ], `has-sub`, 5),
            new Menu(`Hilfe`, `#`, [], ``, 6)
        ];
    }
}