// Wie using => Kurzschreibweise
const { app, BrowserWindow, ipcMain } = require('electron');
// const dbService = require('./server/sql_server').dbService;
let request = require('request');
let path = require('path');

let window;

// TODO SQL-Server auf WIFI-Server als WEBSERVICE auslagern (Service bleibt, knex wird ausgelagert und durch AJAX-Requests an den Server ersetzt)
// TODO Architektur im Lastenheft beschreiben - Server beschreiben
// TODO Architektur - Schema zeichnen mit Server, DB, usw. 
// TODO CI - bei GITLAB ansehen mit .yml Datei
// REVIEW PostInstall erklären!!!
// REVIEW SERVER automatisch starten
// REVIEW AJAX - event.reply erklären
// REVIEW Session Empfehlung für Electron SESSION STORAGE
// REVIEW Auf Seite weiterleiten
// REVIEW Actual Date in Date-Input
// TODO Actueller User aus Session & Language global
// TODO Todos als Issue einbauen (Gitlab)

function newWindow() {
    window = new BrowserWindow({
        width: 1024,
        height: 768,
        frame: true,
        backgroundColor: '#fefefe',
        title: 'TODO - Liste',
        webPreferences: {
            nodeIntegration: true,
            zoomFactor: 1
        },
        icon: path.join(__dirname, 'assets/icons/icon_128.png')
    });

    // window.webContents.openDevTools();

    // let menu = Menu.buildFromTemplate(myMenu);
    // Menu.setApplicationMenu(menu);
    window.loadFile('./app/src/login.html');

    window.on('closed', function () {
        mainWindow = null
    });
    window.once('ready-to-show', () => {
        window.show()
    });
}

app.on('ready', () => {
    newWindow();
    // knex('Users').where('id', 13).del().then()
});

// ipcMain.on('windowLoaded', function() {
//     let result = knex.select('firstname').from('Users');
//     result.then(function(rows) {
//         window.webContents.send('resultSent', rows);
//     });
// })

ipcMain.on('getAccountTypes', function (event) {
    // request.get('http://localhost:8287/finance/accounttypes')
    // dbService.getAccountTypes(event);
});

ipcMain.on('beginSession', function (event, user) {
    event.returnValue = window.loadFile('./app/src/profil.html');
});
ipcMain.on('registerUser', function (event, user) {
    // dbService.registerUser(event, user);
});
ipcMain.on('getUsersAccounts', function (event, userId) {
    // dbService.getUsersAccounts(event, userId);
});
ipcMain.on('createOrUpdateAccount', function (event, account) {
    // dbService.createOrUpdateAccount(event, account);
});
ipcMain.on('deleteAccount', function (event, id) {
    // dbService.deleteAccount(event, id);
});
ipcMain.on('updateUser', function (event, user) {
    // dbService.updateUser(event, user);
});
ipcMain.on('getUserById', function(event, userId) {
    // dbService.getUserById(event, userId);
});

