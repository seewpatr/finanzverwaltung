console.log('SQL - Datenbank-Server gestartet');
// import express from 'express';
// let app = express();
import { join } from 'path';

// let server = app.listen(8286, function () {
//     console.log('DB Server running on port 8286');
// });

let knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: join(__dirname, './financeV2.db')
    },
    pool: {
        afterCreate: (conn, cb) => conn.run('PRAGMA foreign_keys = ON', cb)
    }
});

export const dbService = {
    /**
     * Ladet alle Konto-Arten aus der Datenbank     
     */
    getAccountTypes() {
        console.log(`${this.getTimeStamp(new Date())}Account-Arten geladen`);
        return new Promise((resolve, reject) => {
            knex
                .select()
                .table('AccountTypes')
                .then((data) => {
                    knex('Status')
                        .where({ id: 202, language_id: 2 })
                        .then(function (statusData) {
                            let success = {
                                data: data,
                                status: statusData[0]
                            };
                            resolve(success);
                        });
                });
        });
    },
    /**
     * Loggt einen Benutzer in das System ein und validiert dabei das übergebene Passwort   
     * @param {*} user Erwartet einen Benutzer vom Datentyp Object
     */
    loginUser(user) {
        console.log(`${this.getTimeStamp(new Date())}User - Login angefordert`);

        return new Promise((resolve, reject) => {
            knex('Users')
                .where({
                    username: user.username.toLowerCase()
                })
                .then((data) => {
                    if (data.length == 1) {
                        if (data[0].password == user.password) {
                            console.log(`\t | Login erfolgreich`);
                            knex('Status')
                                .where({ id: 202, language_id: 2 })
                                .then(function (statusData) {
                                    let success = {
                                        data: data[0],
                                        status: statusData[0]
                                    };
                                    resolve(success);
                                });
                        }
                        else {
                            console.log(`\t | Authentifizierung fehlgeschlagen`);
                            knex('Status')
                                .where({ id: 401, language_id: 2 }).then((statusData) => {
                                    let error = {
                                        data: {},
                                        status: statusData[0]
                                    }
                                    reject(error);
                                });
                        }
                    }
                    else {
                        console.log(`\t | User nicht gefunden`);
                        knex('Status')
                            .where({ id: 404, language_id: 2 }).then((statusData) => {
                                let error = {
                                    data: {},
                                    status: statusData[0]
                                }
                                reject(error);
                            });
                    }
                });
        })
    },
    /**
         * Aktualisiert einen Benutzer in der Datenbank und validiert dabei das übergebene Passwort         
         * @param {*} user Erwartet einen Benutzer vom Datentyp Object
         */
    updateUser(user) {
        console.log(`${this.getTimeStamp(new Date())} User - Update angefordert`);
        return new Promise((resolve, reject) => {
            knex('Users')
                .where({ id: user.id, password: user.password })
                .then(function (data) {
                    if (data.length == 1) {
                        console.log('\t | Validierung erfolgreich')
                        knex('Users')
                            .where({ id: user.id })
                            .update({
                                comments: user.comments,
                                firstname: user.firstname,
                                gender: user.gender,
                                lastname: user.lastname,
                            })
                            .then((userId) => {
                                knex('Users').where({ id: userId }).then((user) => {
                                    knex('Status')
                                        .where({ id: 200, language_id: 2 })
                                        .then((statusData) => {
                                            console.log('\t | Update erfolgreich durchgeführt')
                                            let success = {
                                                data: user[0],
                                                status: statusData[0]
                                            };
                                            resolve(success)
                                        });
                                })
                            })
                            .catch(() => {
                                console.log(`\t | Update nicht durchgeführt`);
                                knex('Status')
                                    .where({ id: 400, language_id: 2 }).then((statusData) => {
                                        let error = {
                                            data: user.id,
                                            status: statusData[0]
                                        };
                                        reject(error)
                                    });
                            })
                    }
                    else {
                        console.log(`\t | Authentifizierung fehlgeschlagen`);
                        knex('Status')
                            .where({ id: 401, language_id: 2 }).then((statusData) => {
                                let error = {
                                    data: user.id,
                                    status: statusData[0]
                                };
                                reject(error)
                            });
                    }
                })
        });
    },
    /**
        * Registriert einen Benutzer in der Datenbank und speichert die übergebenen Daten    
        * @param {*} user Erwartet einen Benutzer vom Datentyp Object
        */
    registerUser(user) {
        console.log(`${this.getTimeStamp(new Date())}Registration requested`);
        return new Promise((resolve, reject) => {
            knex('Users').insert({
                email: user.email,
                language_id: 2,
                currency_id: 50,
                colortheme_id: 1,
                role: 'benutzer',
                active: true,
                username: user.email,
                password: user.password
            })
                .then(() => {
                    console.log(`\t | Registrierung erfolgreich`);
                    knex('Status')
                        .where({ id: 201, language_id: 2 }).then((statusData) => {
                            let success = {
                                data: user.email,
                                status: statusData[0]
                            };
                            resolve(success);
                        });
                })
                .catch(() => {
                    knex('Status')
                        .where({ id: 400, language_id: 2 }).then((statusData) => {
                            console.log(`\t | Registrierung nicht erfolgreich`);
                            reject(statusData[0]);
                        });
                })
        });
    },
    // TODO Eventuell alle Benutzer-Daten kombinieren (User, Account, Entries, usw. )
    /**
     * Ladet einen Benutzer anhand der übergebenen ID aus der Datenbank     
     * @param {integer} userId Erwartet eine Benutzer-Id vom Datentyp Integer
     */
    getUserById(userId) {
        console.log(`${this.getTimeStamp(new Date())}User laden angefordert`);
        return new Promise((resolve, reject) => {
            knex('Users')
                .where({ id: userId })
                .then((data) => {
                    if (data.length == 1) {
                        this.getUsersAccounts(userId)
                            .then((userAccountsData) => {
                                knex('Status')
                                    .where({ id: 202, language_id: 2 }).then((statusData) => {
                                        console.log(`\t | User erfolgreich geladen`);
                                        let success = {
                                            data: data[0],
                                            accountsData: userAccountsData,
                                            status: statusData[0]
                                        };
                                        resolve(success);
                                    });
                            })
                            .catch(() => {
                                knex('Status')
                                .where({ id: 400, language_id: 2 }).then((statusData) => {
                                    console.log(`\t | Accounts konnten nicht geladen werden`);
                                    reject(statusData[0]);
                                });
                            })
                    }
                    else {
                        knex('Status')
                            .where({ id: 404, language_id: 2 }).then((statusData) => {
                                console.log(`\t | User konnte nicht gefunden werden`);
                                reject(statusData[0]);
                            });
                    }
                })
                .catch(() => {
                    knex('Status')
                        .where({ id: 400, language_id: 2 }).then((statusData) => {
                            console.log(`\t | User konnte nicht geladen werden`);
                            reject(statusData[0]);
                        });
                });
        });
    },
    /**
    * Ladet alle zu einem Benutzer gehörenden Konten anhand der übergebenen ID aus der Datenbank    
    * @param {integer} userId Erwartet eine Benutzer-Id vom Datentyp Integer
    */
    getUsersAccounts(userId) {
        console.log(`${this.getTimeStamp(new Date())}Accounts geladen`);
        return new Promise((resolve, reject) => {
            knex('Accounts')
                .where({ user_id: userId })
                .then((data) => {
                    resolve(data);
                })
                .catch(() => {
                    reject();
                });
        });
    },
    /**
    * Erstellt oder aktualisiert ein zu einem Benutzer gehörendes Konto    
    * @param {*} account Erwartet ein Konto vom Datentyp Object
    */
    createOrUpdateAccount(account) {
        console.log(`${this.getTimeStamp(new Date())}Create or Update Account requested`);
        return new Promise((resolve, reject) => {
            knex('Accounts')
                .where({ id: account.id })
                .then((data) => {
                    if (data.length == 1) {
                        (
                            knex('Accounts')
                                .where({ id: account.id })
                                .update(account)
                                .then((data) => {
                                    knex('Status')
                                        .where({ id: 200, language_id: 2 })
                                        .then((statusData) => {
                                            let success = {
                                                data: data,
                                                status: statusData[0]
                                            }
                                            console.log(`\t | Account erfolgreich aktualisiert`);
                                            resolve(success);
                                        });
                                })
                                .catch((data) => {
                                    knex('Status')
                                        .where({ id: 400, language_id: 2 }).then((statusData) => {
                                            console.log(`\t | Account konnte nicht aktualisiert werden`);
                                            reject(statusData[0]);
                                        });
                                })
                        )
                    }
                    else {
                        (
                            knex('Accounts')
                                .insert(account)
                                .then((data) => {
                                    knex('Status')
                                        .where({ id: 201, language_id: 2 })
                                        .then((statusData) => {
                                            let success = {
                                                data: data,
                                                status: statusData[0]
                                            }
                                            console.log(`\t | Neuer Account angelegt`);
                                            resolve(success);
                                        });
                                })
                                .catch((data) => {
                                    knex('Status')
                                        .where({ id: 400, language_id: 2 })
                                        .then((statusData) => {
                                            console.log(`\t | Account konnte nicht angelegt werden`);
                                            reject(statusData[0]);
                                        });
                                })
                        );
                    }
                })
        })
    },
    /**
   * Löscht ein existierendes zu einem Benutzer gehörendes Konto aus der Datenbank   
   * @param {integer} accountId Erwartet eine Konto-Id vom Datentyp Integer
   */
    deleteAccount(accountId) {
        console.log(`${this.getTimeStamp(new Date())}Delete Account requested`);
        return new Promise((resolve, reject) => {
            knex('Accounts')
                .where({ id: accountId })
                .del()
                .then((data) => {
                    knex('Status')
                        .where({ id: 202, language_id: 2 })
                        .then((statusData) => {
                            let success = {
                                data: data,
                                status: statusData
                            }
                            console.log(`\t | Account wurde erfolgreich gelöscht`);
                            resolve(success);
                        })
                })
                .catch((data) => {
                    knex('Status')
                        .where({ id: 400, language_id: 2 })
                        .then((statusData) => {
                            console.log(`\t | Account konnte nicht gelöscht werden`);
                            reject(statusData[0]);
                        });
                });
        });
    },
    getStatus(statuscode, language_id) {
        return new Promise((resolve, reject) => {
            knex('Status')
                .where({ id: statuscode, language_id: language_id })
                .then((statusData) => {
                    resolve(statusData[0]);
                })
                .catch((statusData) => {
                    reject();
                });
        });
    },
    /**
     * Konvertiert ein Datumsobjekt in einen passenden Timestamp-String
     * @param {date} date Erwartet ein Datumsobjekt vom Datentyp Date
     */
    getTimeStamp(date) {
        let hours = ('0' + date.getHours()).slice(-2);
        let minutes = ('0' + date.getMinutes()).slice(-2);
        let seconds = ('0' + date.getSeconds()).slice(-2);
        return `${hours}:${minutes}:${seconds} | `;
    }
}