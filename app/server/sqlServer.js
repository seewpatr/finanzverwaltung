console.log('Datenbank Server Script');

// npm install express <= wenn nicht vorhanden
import express from 'express';
import { urlencoded } from 'body-parser';
import { join } from 'path';
import { dbService } from './dbService';


let app = express();

// let knex = require('knex')({
//   client: 'sqlite3',
//   connection: {
//     filename: join(__dirname, './financeV2.db')
//   },
//   pool: {
//     afterCreate: (conn, cb) => conn.run('PRAGMA foreign_keys = ON', cb)
//   }
// });

app.use(
  (request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, DELETE, POST, PUT');
    response.setHeader('Content-Type', 'application/json');
    next();
  },
  urlencoded({ extended: true })
);

let server = app.listen(8287, () => {
  console.log('DB Server running on port 8287');
});

app.get('/finance/accounttypes', (request, response) => {
  dbService
    .getAccountTypes()
    .then((data) => {
      response.end(JSON.stringify(data));
    });
});

app.get('/finance/user/:id', (request, response) => {
  let userId = request.params.id;

  dbService
    .getUserById(userId)
    .then((data) => {
      response.end(JSON.stringify(data));
    })
});

app.post('/finance/loginUser', (request, response) => {
  let user = request.body;

  dbService
    .loginUser(user)
    .then((loginUser) => {
      response.end(JSON.stringify(loginUser));
    })
    .catch((error) => {
      console.log(error);
      response.end(JSON.stringify(error));
    });
});

app.post('finance/updateUser', (request, response) => {
  let user = request.body;

  dbService
    .updateUser2(user)
    .then(function (updatedUser) {
      // knex('Status')
      //   .where({ id: 200, language_id: 2 })
      //   .then(function (statusData) {
      //     console.log('\t | Update erfolgreich durchgeführt')
      //     let success = {
      //       data: user[0],
      //       status: statusData[0]
      //     };

      //   });
      response.end(JSON.stringify(updatedUser));
    })
    .catch((status) => {
      // knex('Status')
      // .where({ id: 400, language_id: 2 }).then(function (statusData) {
      //     let error = {
      //         data: user.id,
      //         status: statusData[0]
      //     };
      //     event.reply('error', error)
      // });

      // knex('Status')
      // .where({ id: 401, language_id: 2 }).then(function (statusData) {
      //     let error = {
      //         data: user.id,
      //         status: statusData[0]
      //     };
      //     event.reply('error', error)
      // });
    });
});

app.post('finance/registerUser', (request, response) => {
  let user = request.body;

  dbService
    .registerUser2(user)
});


