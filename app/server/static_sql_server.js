console.log('SQL - Datenbank-Server gestartet');
const { ipcMain } = require('electron');
let express = require('express');
let app = express();
let path = require('path');

let server = app.listen(8286, function () {
    console.log('DB Server running on port 8286');
});

let knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, './financeV2.db')
    },
    pool: {
        afterCreate: (conn, cb) => conn.run('PRAGMA foreign_keys = ON', cb)
    }
});

// connection('Currencies').del().then();
// knex('Status').where('id' ,202).update({
//     message: 'Die Anfrage wurde erfolgreich durchgeführt'
// }).then()

// knex('Status').where('id', 201).update({
//     message: 'Die Ressource wurde erfolgreich erstellt'
// }).then()

exports.dbService = {    
    /**
     * Ladet alle Konto-Arten aus der Datenbank
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     */
    getAccountTypes(event) {
        console.log(`${this.getTimeStamp(new Date())}Account-Arten geladen`);
        knex('AccountTypes').then(function (data) {
            event.returnValue = data;
        })
    },
    /**
     * Loggt einen Benutzer in das System ein und validiert dabei das übergebene Passwort
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {*} user Erwartet einen Benutzer vom Datentyp Object
     */
    loginUser(event, user) {
        console.log(`${this.getTimeStamp(new Date())}Login requested`);
        knex('Users')
            .where('username'.toLowerCase(), user.username.toLowerCase())
            .then(function (data) {
                if (data.length == 1) {
                    if (data[0].password == user.password) {
                        console.log(`\t | Login erfolgreich`);
                        knex('Status')
                            .where({ id: 202, language_id: 2 })
                            .then(function (statusData) {
                                let success = {
                                    data: data[0],
                                    status: statusData[0]
                                };
                                event.reply('loginSuccess', success);
                            });
                    }
                    else {
                        console.log(`\t | Authentifizierung fehlgeschlagen`);
                        knex('Status')
                            .where({ id: 401, language_id: 2 }).then(function (data) {
                                event.reply('error', data[0]);
                            });
                    }
                }
                else {
                    console.log(`\t | User nicht gefunden`);
                    knex('Status')
                        .where({ id: 404, language_id: 2 }).then(function (data) {
                            event.reply('error', data[0]);
                        });
                }
            });
    },
    /**
     * Aktualisiert einen Benutzer in der Datenbank und validiert dabei das übergebene Passwort
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {*} user Erwartet einen Benutzer vom Datentyp Object
     */
    updateUser(event, user) {
        console.log(`${this.getTimeStamp(new Date())} Update User requested`);

        knex('Users')
            .where({ id: user.id, password: user.password })
            .then(function (data) {
                if (data.length > 0) {
                    console.log('\t | Validierung erfolgreich')
                    knex('Users')
                        .where({ id: user.id })
                        .update({
                            comments: user.comments,
                            firstname: user.firstname,
                            gender: user.gender,
                            lastname: user.lastname,
                        })
                        .then(function (userId) {
                            knex('Users').where({ id: userId }).then(function (user) {
                                knex('Status')
                                    .where({ id: 200, language_id: 2 })
                                    .then(function (statusData) {
                                        console.log('\t | Update erfolgreich durchgeführt')
                                        let success = {
                                            data: user[0],
                                            status: statusData[0]
                                        };
                                        event.reply('updateUserSuccess', success)
                                    });
                            })
                        })
                        .catch(function (data) {
                            console.log(`\t | Update nicht durchgeführt`);
                            knex('Status')
                                .where({ id: 400, language_id: 2 }).then(function (statusData) {
                                    let error = {
                                        data: user.id,
                                        status: statusData[0]
                                    };
                                    event.reply('error', error)
                                });
                        })
                }
                else {
                    console.log(`\t | Authentifizierung fehlgeschlagen`);
                    knex('Status')
                        .where({ id: 401, language_id: 2 }).then(function (statusData) {
                            let error = {
                                data: user.id,
                                status: statusData[0]
                            };
                            event.reply('error', error)
                        });
                }
            })
    },
    /**
     * Registriert einen Benutzer in der Datenbank und speichert die übergebenen Daten
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {*} user Erwartet einen Benutzer vom Datentyp Object
     */
    registerUser(event, user) {
        console.log(`${this.getTimeStamp(new Date())}Registration requested`);
        knex('Users').insert({
            email: user.email,
            language_id: 2,
            currency_id: 50,
            colortheme_id: 1,
            role: 'benutzer',
            active: true,
            username: user.email,
            password: user.password
        })
            .then(function () {
                console.log(`\t | Registrierung erfolgreich`);
                knex('Status')
                    .where({ id: 201, language_id: 2 }).then(function (statusData) {
                        let success = {
                            data: user.email,
                            status: statusData[0]
                        };
                        event.reply('registerSuccess', success)
                    });
            })
            .catch(function (data) {
                knex('Status')
                    .where({ id: 400, language_id: 2 }).then(function (statusData) {
                        console.log(`\t | Registrierung nicht erfolgreich`);
                        event.reply('error', statusData[0]);
                    });
            })
    },
    // TODO Eventuell alle Benutzer-Daten kombinieren (User, Account, Entries, usw. )
    /**
     * Ladet einen Benutzer anhand der übergebenen ID aus der Datenbank
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {integer} userId Erwartet eine Benutzer-Id vom Datentyp Integer
     */
    getUserById(event, userId) {
        console.log(`${this.getTimeStamp(new Date())}User geladen`);
        knex('Users')
            .where({ id: userId })
            .then(function (data) {
                event.returnValue = data;
            });
    },
    /**
     * Ladet alle zu einem Benutzer gehörenden Konten anhand der übergebenen ID aus der Datenbank
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {integer} userId Erwartet eine Benutzer-Id vom Datentyp Integer
     */
    getUsersAccounts(event, userId) {
        console.log(`${this.getTimeStamp(new Date())}Accounts geladen`);
        knex('Accounts')
            .where({ user_id: userId })
            .then(function (data) {
                event.returnValue = data;
            });
    },
    /**
     * Erstellt oder aktualisiert ein zu einem Benutzer gehörendes Konto
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {*} account Erwartet ein Konto vom Datentyp Object
     */
    createOrUpdateAccount(event, account) {
        console.log(`${this.getTimeStamp(new Date())}Create or Update Account requested`);

        knex('Accounts')
            .where({ id: account.id })
            .then(function (data) {
                data.length > 0
                    ?
                    (
                        knex('Accounts')
                            .where({ id: account.id })
                            .update(account)
                            .then(function (data) {
                                knex('Status')
                                    .where({ id: 200, language_id: 2 })
                                    .then(function (statusData) {
                                        let success = {
                                            data: data,
                                            status: statusData[0]
                                        }
                                        console.log(`\t | Account erfolgreich aktualisiert`);
                                        event.reply('accountCreatedOrUpdated', success);
                                    });
                            })
                            .catch(function (data) {
                                knex('Status')
                                    .where({ id: 400, language_id: 2 }).then(function (statusData) {
                                        console.log(`\t | Account konnte nicht aktualisiert werden`);
                                        event.reply('error', statusData[0]);
                                    });
                            })
                    )
                    :
                    (
                        knex('Accounts')
                            .insert(account)
                            .then(function (data) {
                                knex('Status')
                                    .where({ id: 201, language_id: 2 })
                                    .then(function (statusData) {
                                        let success = {
                                            data: data,
                                            status: statusData[0]
                                        }
                                        console.log(`\t | Neuer Account angelegt`);
                                        event.reply('accountCreatedOrUpdated', success);
                                    });
                            })
                            .catch(function (data) {
                                knex('Status')
                                    .where({ id: 400, language_id: 2 })
                                    .then(function (statusData) {
                                        console.log(`\t | Account konnte nicht angelegt werden`);
                                        event.reply('error', statusData[0]);
                                    });
                            })
                    );
            })
    },
    /**
     * Löscht ein existierendes zu einem Benutzer gehörendes Konto aus der Datenbank
     * @param {*} event Erwartet ein Event-Objekt vom Datentyp Event-Listener
     * @param {integer} accountId Erwartet eine Konto-Id vom Datentyp Integer
     */
    deleteAccount(event, accountId) {
        knex('Accounts')
            .where({ id: accountId })
            .del()
            .then(function (data) {
                knex('Status')
                    .where({ id: 202, language_id: 2 })
                    .then(function (statusData) {
                        let success = {
                            data: data,
                            status: statusData
                        }
                        console.log(`\t | Account wurde erfolgreich gelöscht`);
                        event.reply('accountDeleted', success);
                    })
            })
            .catch(function (data) {
                knex('Status')
                    .where({ id: 400, language_id: 2 })
                    .then(function (statusData) {
                        console.log(`\t | Account konnte nicht gelöscht werden`);
                        event.reply('error', statusData[0]);
                    });
            });
    },
    /**
     * Konvertiert ein Datumsobjekt in einen passenden Timestamp-String
     * @param {date} date Erwartet ein Datumsobjekt vom Datentyp Date
     */
    getTimeStamp(date) {
        let hours = ('0' + date.getHours()).slice(-2);
        let minutes = ('0' + date.getMinutes()).slice(-2);
        let seconds = ('0' + date.getSeconds()).slice(-2);
        return `${hours}:${minutes}:${seconds} | `;
    }
    //,
    // getStatus(statuscode, language_id) {
    //     return new Promise((resolve, reject) => {
    //         knex('Status')
    //         .where({ id: statuscode, language_id: language_id })
    //         .then((statusData) => {              
    //             resolve(statusData[0]);
    //         })
    //         .catch((statusData) => {
    //             reject();
    //         });
    //     });
    // }
}